# drone navigation package

This package provides a navigationhandler for a 2 propeller sea-drone.

## Installation

```bash
yarn
```

## Usage

```javascript
const navigationHandler = require("./dist/navigationHandler").navigationHandler;

// example mock data

// control unit
const control = {
  getPowerLeft: () => 1,
  getPowerRight: () => 1,
  setPowerLeft: value => {},
  setPowerRight: value => {},
};

// gps unit
const position = {
  getHeading: () => 0,
  getPosition: () => {
    return { latitude: 11, longitude: 12 };
  },
};

// user commands : START | RETURN | STOP | TEST_LEFT | TEST_RIGHT | TEST_FORWARD | TEST_TURN_LEFT | TEST_TURN_RIGHT
const command = "START";

// map unit
const coordinates = [
  { longitude: 52.441604, latitude: 13.627809 },
  { longitude: 52.440854, latitude: 13.627809 },
  { longitude: 52.441604, latitude: 13.627609 },
  { longitude: 52.440854, latitude: 13.627609 },
  { longitude: 52.441604, latitude: 13.627309 },
  { longitude: 52.440854, latitude: 13.627309 },
];

// image recognition unit
const trash = [];

// command unit
setInterval(() => {
  return navigationHandler({
    control,
    position,
    command,
    coordinates,
    trash,
    // enables random behavior
    enableRandom: true,
  });
}, 100);
```
