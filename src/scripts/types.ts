export type Coordinate = {
  longitude: number;
  latitude: number;
};

export type Trash = {
  direction: number;
};

export type Position = {
  /**
   * Returns the current `position`
   * @returns {Coordinate} - the current position
   */
  getPosition: () => Coordinate;
  /**
   * Returns the current `heading` in `degress(0-360)`
   * @returns {number} - the current `heading`
   */
  getHeading: () => number;
};

export type Control = {
  /**
   * Returns the current `velocity`
   * @returns {number} - the current `velocity`
   */
  getPowerLeft: () => number;
  /**
   * Returns the current `velocity`
   * @returns {number} - the current `velocity`
   */
  getPowerRight: () => number;
  /**
   * Sets the current `velocity`
   * @param velocity - value should be -`1<=x<=1`
   */
  setPowerLeft: (velocity: number) => void;

  /**
   * Sets the current `velocity`
   * @param velocity - value should be -`1<=x<=1`
   */
  setPowerRight: (velocity: number) => void;
};

export type UserCommands =
  | "START"
  | "STOP"
  | "RETURN"
  | "TEST_LEFT"
  | "TEST_RIGHT"
  | "TEST_FORWARD"
  | "TEST_TURN_LEFT"
  | "TEST_TURN_RIGHT";
