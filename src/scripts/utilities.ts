import {isEmpty} from "lodash";
import {Trash} from "./types";

export const trashDetectionChecker = (trash: Trash[]): false | Trash => {
  if (!isEmpty(trash)) return trash[0];
  return false;
};
