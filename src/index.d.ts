declare module "sea-drone-simulation" {
  interface simulationConfig {
    loop: Function;
    /**
     * @default element = document.body
     */
    element?: HTMLElement;
    /**
     * @default wind =  {x:0,y:0}
     */
    wind?: {x: number; y: number};
    /**
     * Is the top left point of the map and is used together with bottomRightGeoPointMap to create a rectangle which is used as the map
     * @default topLeftGeoPointMap = {longitude: 52.440704, latitude: 13.627919}
     */
    topLeftGeoPointMap?: object;
    /**
     * * Is the bottom right point of the map and is used together with topLeftGeoPointMap to create a rectangle which is used as the map
     * @default bottomRightGeoPointMap = {longitude: 52.441704, latitude: 13.626919}
     */
    bottomRightGeoPointMap?: object;
    /**
     * @default numberOfBottles = 3
     */
    numberOfBottles?: number;
    /**
     * @default detectorAngle = 45
     */
    detectorAngle?: number;
    /**
     * @default detectorRange = 80
     */
    detectorRange?: number;
  }
  export const createSimulation: ({
    ...args
  }: simulationConfig) => {start: () => {}};
  type Coordinate = {
    longitude: number;
    latitude: number;
  };

  type Trash = {
    direction: number;
    distance: 0 | 1;
  };

  export interface Position {
    /**
     * Returns the current `position`
     * @returns {Coordinate} - the current position
     */
    getPosition: () => Coordinate;
    /**
     * Returns the current `heading` in `degress(0-360)`
     * @returns {number} - the current `heading`
     */
    getHeading: () => number;
  }
  export interface Map {
    /**
     * Returns the fence of the simulation as `tuple`
     * @returns {[Coordinate,Coordinate]} - the `topLeftGeoPointMap` corner && `bottomRightGeoPointMap` corner
     */
    getFence: () => [Coordinate, Coordinate];
  }

  export interface Control {
    /**
     * Returns the current `velocity`
     * @returns {number} - the current `velocity`
     */
    getPowerLeft: () => number;
    /**
     * Returns the current `velocity`
     * @returns {number} - the current `velocity`
     */
    getPowerRight: () => number;
    /**
     * Sets the current `velocity`
     * @param velocity - value should be -`1<=x<=1`
     */
    setPowerLeft: (velocity: number) => void;

    /**
     * Sets the current `velocity`
     * @param velocity - value should be -`1<=x<=1`
     */
    setPowerRight: (velocity: number) => void;
  }
  export interface Detector {
    /**
     * Gives back an array with all the detected trash
     * @returns {[Trash?]} - array of detected trash
     */
    getDetectedWaste: () => [Trash?];
  }

  export interface Container {
    /**
     * Gives back an array with all the detected trash
     * @returns {number} - weight of collect trash
     */
    getWeight: () => number;
  }
}
