"use strict";
exports.__esModule = true;
var navigationMethods_1 = require("../navigationMethods");
var states_1 = require("../states");
var reachSearchPointHandler = function (currentPosition, control) {
    if (navigationMethods_1.calcDistanceToCoordinate(currentPosition, states_1.SEARCH.TARGET) <=
        states_1.SEARCH.TARGET_REACHED_TOLERANCE) {
        console.log("REACHED_POINT");
        states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_MIN);
        states_1.SEARCH.setPathIndex(states_1.SEARCH.PATH_INDEX + 1);
        states_1.SEARCH.setTarget(null);
        states_1.NAVIGATION.setState(null);
    }
};
exports.searchNavigationController = function (control, position) {
    var targetCourse;
    var currentHeading = position.getHeading();
    var currentPosition = position.getPosition();
    switch (states_1.NAVIGATION.STATE) {
        case states_1.navigationStates.CONFIG_SEARCH:
            console.log("CONFIG_SEARCH");
            states_1.SEARCH.setTarget(states_1.SEARCH.PATH[states_1.SEARCH.PATH_INDEX]);
            targetCourse = navigationMethods_1.calcAngleToCoordinate(currentPosition, states_1.SEARCH.TARGET);
            states_1.NAVIGATION.setCourse(targetCourse);
            states_1.NAVIGATION.setState(states_1.navigationStates.TURN_SEARCH);
            break;
        case states_1.navigationStates.TURN_SEARCH:
            console.log("TURN_SEARCH");
            console.log("SEARCH_COURSE", states_1.NAVIGATION.COURSE);
            console.log("CURRENT_HEADING", currentHeading);
            if (states_1.NAVIGATION.COURSE - states_1.NAVIGATION.COURSE_TOLERANCE < currentHeading &&
                currentHeading < states_1.NAVIGATION.COURSE + states_1.NAVIGATION.COURSE_TOLERANCE) {
                states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_HIGH);
                states_1.NAVIGATION.setState(states_1.navigationStates.STRAIGHT_POINT);
            }
            else {
                states_1.BOAT.setBoatTurnOnSpotRight(control, states_1.BOAT.SPEED_HIGH);
            }
            break;
        case states_1.navigationStates.STRAIGHT_POINT:
            console.log("STRAIGHT_POINT");
            states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_HIGH);
            targetCourse = navigationMethods_1.calcAngleToCoordinate(currentPosition, states_1.SEARCH.TARGET);
            navigationMethods_1.courseCorrectionHandler(currentHeading, targetCourse, control);
            reachSearchPointHandler(currentPosition, control);
            break;
    }
};
//# sourceMappingURL=search.js.map