"use strict";
exports.__esModule = true;
var navigationMethods_1 = require("../navigationMethods");
var states_1 = require("../states");
var reachRandomTargetHandler = function (currentPosition, control) {
    if (navigationMethods_1.calcDistanceToCoordinate(currentPosition, states_1.RANDOM.TARGET) <=
        states_1.RANDOM.TARGET_REACHED_TOLERANCE) {
        console.log("REACHED_RANDOM_POINT");
        states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_MIN);
        states_1.RANDOM.setTarget(null);
        states_1.NAVIGATION.setState(null);
    }
};
exports.randomNavigationController = function (control, position) {
    var targetCourse;
    var currentHeading = position.getHeading();
    var currentPosition = position.getPosition();
    switch (states_1.NAVIGATION.STATE) {
        case states_1.navigationStates.CONFIG_RANDOM:
            console.log("CONFIG_RANDOM");
            var targetInsideBounds = navigationMethods_1.calcRandomCoordInsideBounds(states_1.BOUNDARY.BOUNDS);
            console.log("CONFIG_RANDOM_TARGET", targetInsideBounds);
            targetCourse = navigationMethods_1.calcAngleToCoordinate(currentPosition, targetInsideBounds);
            console.log("CONFIG_RANDOM_TARGET_COURSE", targetCourse);
            states_1.NAVIGATION.setCourse(targetCourse);
            states_1.RANDOM.setTarget(targetInsideBounds);
            states_1.NAVIGATION.setState(states_1.navigationStates.TURN_RANDOM);
            break;
        case states_1.navigationStates.TURN_RANDOM:
            console.log("TURN_RANDOM");
            console.log("RANDOM_COURSE", states_1.NAVIGATION.COURSE);
            console.log("CURRENT_HEADING", currentHeading);
            if (states_1.NAVIGATION.COURSE - states_1.NAVIGATION.COURSE_TOLERANCE < currentHeading &&
                currentHeading < states_1.NAVIGATION.COURSE + states_1.NAVIGATION.COURSE_TOLERANCE) {
                states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_HIGH);
                states_1.NAVIGATION.setState(states_1.navigationStates.STRAIGHT_RANDOM);
            }
            else {
                states_1.BOAT.setBoatTurnOnSpotRight(control, states_1.BOAT.SPEED_HIGH);
            }
            break;
        case states_1.navigationStates.STRAIGHT_RANDOM:
            console.log("STRAIGHT_RANDOM");
            states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_LOW);
            targetCourse = navigationMethods_1.calcAngleToCoordinate(currentPosition, states_1.RANDOM.TARGET);
            navigationMethods_1.courseCorrectionHandler(currentHeading, targetCourse, control);
            reachRandomTargetHandler(currentPosition, control);
            break;
        default:
            break;
    }
};
//# sourceMappingURL=random.js.map