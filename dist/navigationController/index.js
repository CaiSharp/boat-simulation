"use strict";
exports.__esModule = true;
var boundary_1 = require("./boundary");
exports.boundaryNavigationController = boundary_1.boundaryNavigationController;
var search_1 = require("./search");
exports.searchNavigationController = search_1.searchNavigationController;
var trash_1 = require("./trash");
exports.trashNavigationController = trash_1.trashNavigationController;
var random_1 = require("./random");
exports.randomNavigationController = random_1.randomNavigationController;
//# sourceMappingURL=index.js.map