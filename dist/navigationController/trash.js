"use strict";
exports.__esModule = true;
var navigationMethods_1 = require("../navigationMethods");
var states_1 = require("../states");
var utilities_1 = require("../utilities");
exports.trashNavigationController = function (_a) {
    var control = _a.control, position = _a.position, trashGroup = _a.trashGroup;
    var targetCourse;
    var trash;
    var currentHeading = position.getHeading();
    switch (states_1.NAVIGATION.STATE) {
        case states_1.navigationStates.CONFIG_TRASH:
            console.log("CONFIG_TRASH");
            states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_MIN);
            trash = utilities_1.trashDetectionChecker(trashGroup);
            if (trash) {
                states_1.NAVIGATION.setState(states_1.navigationStates.TURN_TRASH);
            }
            else {
                states_1.NAVIGATION.setState(states_1.navigationStates.CONFIG_SEARCH);
            }
            break;
        case states_1.navigationStates.TURN_TRASH:
            console.log("TURN_TRASH");
            console.log("TRASH_COURSE", states_1.NAVIGATION.COURSE);
            console.log("CURRENT_HEADING", currentHeading);
            trash = utilities_1.trashDetectionChecker(trashGroup);
            if (trash) {
                switch (navigationMethods_1.determineTrashTurnDirection(trash.direction, currentHeading, states_1.NAVIGATION.COURSE_TOLERANCE)) {
                    case navigationMethods_1.turnDirections.STRAIGHT:
                        states_1.NAVIGATION.setState(states_1.navigationStates.STRAIGHT_TRASH);
                        break;
                    case navigationMethods_1.turnDirections.LEFT:
                        states_1.BOAT.setBoatTurnOnSpotLeft(control, states_1.BOAT.SPEED_HIGH);
                        break;
                    case navigationMethods_1.turnDirections.RIGHT:
                        states_1.BOAT.setBoatTurnOnSpotRight(control, states_1.BOAT.SPEED_HIGH);
                }
            }
            else {
                states_1.NAVIGATION.setState(null);
            }
            break;
        case states_1.navigationStates.STRAIGHT_TRASH:
            console.log("STRAIGHT_TRASH");
            states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_HIGH);
            trash = utilities_1.trashDetectionChecker(trashGroup);
            if (trash) {
                targetCourse = navigationMethods_1.calcTrashAngle(trash.direction, currentHeading);
                navigationMethods_1.courseCorrectionHandler(currentHeading, targetCourse, control);
            }
            else {
                states_1.NAVIGATION.setState(null);
            }
            break;
        default:
            break;
    }
};
//# sourceMappingURL=trash.js.map