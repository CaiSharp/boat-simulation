"use strict";
exports.__esModule = true;
var navigationMethods_1 = require("../navigationMethods");
var states_1 = require("../states");
var reachTargetBoundaryHandler = function (currentPosition, control) {
    if (navigationMethods_1.calcDistanceToCoordinate(currentPosition, states_1.BOUNDARY.TARGET) <=
        states_1.BOUNDARY.TARGET_REACHED_TOLERANCE) {
        console.log("REACHED_TARGET_POINT_IN_BOUNDARY");
        states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_MIN);
        states_1.BOUNDARY.setTarget(null);
        states_1.NAVIGATION.setState(null);
    }
};
exports.boundaryNavigationController = function (control, position) {
    var targetCourse;
    var targetInsideBounds;
    var currentHeading = position.getHeading();
    var currentPosition = position.getPosition();
    switch (states_1.NAVIGATION.STATE) {
        case states_1.navigationStates.CONFIG_BOUNDARY:
            console.log("CONFIG_BOUNDARY");
            if (states_1.RANDOM.IS_ENABLED) {
                targetInsideBounds = navigationMethods_1.calcRandomCoordInsideBounds(states_1.BOUNDARY.BOUNDS);
                console.log("BOUNDARY_TARGET_RANDOM", targetInsideBounds);
                targetCourse = navigationMethods_1.calcAngleToCoordinate(currentPosition, targetInsideBounds);
                console.log("BOUNDARY_COURSE_RANDOM", targetCourse);
            }
            else {
                targetInsideBounds = states_1.SEARCH.TARGET;
                console.log("BOUNDARY_TARGET_SEARCH", targetInsideBounds);
                targetCourse = navigationMethods_1.calcAngleToCoordinate(currentPosition, targetInsideBounds);
                console.log("BOUNDARY_COURSE_SEARCH", targetCourse);
            }
            states_1.NAVIGATION.setCourse(targetCourse);
            states_1.BOUNDARY.setTarget(targetInsideBounds);
            states_1.NAVIGATION.setState(states_1.navigationStates.TURN_BOUNDARY);
            break;
        case states_1.navigationStates.TURN_BOUNDARY:
            console.log("TURN_BOUNDARY");
            console.log("BOUNDARY_COURSE", states_1.NAVIGATION.COURSE);
            console.log("CURRENT_HEADING", currentHeading);
            if (states_1.NAVIGATION.COURSE - states_1.NAVIGATION.COURSE_TOLERANCE < currentHeading &&
                currentHeading < states_1.NAVIGATION.COURSE + states_1.NAVIGATION.COURSE_TOLERANCE) {
                states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_HIGH);
                states_1.NAVIGATION.setState(states_1.navigationStates.STRAIGHT_BOUNDARY);
            }
            else {
                states_1.BOAT.setBoatTurnOnSpotRight(control, states_1.BOAT.SPEED_HIGH);
            }
            break;
        case states_1.navigationStates.STRAIGHT_BOUNDARY:
            console.log("STRAIGHT_BOUNDARY");
            states_1.BOAT.setBoatStraightAhead(control, states_1.BOAT.SPEED_LOW);
            targetCourse = navigationMethods_1.calcAngleToCoordinate(currentPosition, states_1.BOUNDARY.TARGET);
            navigationMethods_1.courseCorrectionHandler(currentHeading, targetCourse, control);
            reachTargetBoundaryHandler(currentPosition, control);
            break;
        default:
            break;
    }
};
//# sourceMappingURL=boundary.js.map