"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var navigationHandler_1 = require("./navigationHandler");
var sea_drone_simulation_1 = require("sea-drone-simulation");
var testData_1 = require("./testData");
var simulation = sea_drone_simulation_1.createSimulation({
    element: document.getElementById("pool"),
    loop: function (value) {
        return navigationHandler_1.navigationHandler(__assign(__assign({}, value), { command: "START", coordinates: testData_1.testCoordinates, enableRandom: true }));
    },
    numberOfBottles: 60,
    topLeftGeoPointMap: { longitude: 52.440704, latitude: 13.627919 },
    bottomRightGeoPointMap: { longitude: 52.441704, latitude: 13.626919 }
});
simulation.start();
//# sourceMappingURL=index.js.map