"use strict";
exports.__esModule = true;
var states_1 = require("./states");
function calcAngleToCoordinate(currentPosition, targetPoint) {
    var boatLong = currentPosition.longitude, boatLat = currentPosition.latitude;
    var targetLong = targetPoint.longitude, targetLat = targetPoint.latitude;
    var opposite = Math.abs(targetLat - boatLat);
    var adjacent = Math.abs(targetLong - boatLong);
    var hypotenuse = Math.hypot(opposite, adjacent);
    var cosOfAngleX = opposite / hypotenuse;
    var angleToTarget = (Math.acos(cosOfAngleX) * 180) / Math.PI;
    if (targetLong - boatLong > 0) {
        if (targetLat - boatLat > 0) {
            return 180 - angleToTarget;
        }
        if (targetLat - boatLat < 0) {
            return angleToTarget;
        }
    }
    if (targetLong - boatLong < 0) {
        if (targetLat - boatLat > 0) {
            return 180 + angleToTarget;
        }
        if (targetLat - boatLat < 0) {
            return 360 - angleToTarget;
        }
    }
    if (targetLong - boatLong === 0) {
        if (targetLat - boatLat > 0) {
            return 180;
        }
        if (targetLat - boatLat < 0) {
            return 0;
        }
        if (targetLat === boatLat) {
            return undefined;
        }
    }
    return undefined;
}
exports.calcAngleToCoordinate = calcAngleToCoordinate;
function calcTrashAngle(direction, currentHeading) {
    var angleSum = currentHeading + direction;
    return (angleSum + 360) % 360;
}
exports.calcTrashAngle = calcTrashAngle;
var turnDirections;
(function (turnDirections) {
    turnDirections["LEFT"] = "LEFT";
    turnDirections["RIGHT"] = "RIGHT";
    turnDirections["STRAIGHT"] = "STRAIGHT";
})(turnDirections = exports.turnDirections || (exports.turnDirections = {}));
function determineTrashTurnDirection(relativeaAngle, currentHeading, courseTolerance) {
    var absoluteAngle = calcTrashAngle(relativeaAngle, currentHeading);
    if (absoluteAngle - courseTolerance < currentHeading &&
        currentHeading < absoluteAngle + courseTolerance) {
        return turnDirections.STRAIGHT;
    }
    if (relativeaAngle < 0) {
        return turnDirections.LEFT;
    }
    if (relativeaAngle > 0) {
        return turnDirections.RIGHT;
    }
}
exports.determineTrashTurnDirection = determineTrashTurnDirection;
var distanceUnit;
(function (distanceUnit) {
    distanceUnit["m"] = "meter";
    distanceUnit["k"] = "kilometer";
})(distanceUnit = exports.distanceUnit || (exports.distanceUnit = {}));
function calcDistanceToCoordinate(originCoord, targetCoord, unit) {
    var R = 6371;
    var dLat = convertDegToRad(targetCoord.latitude - originCoord.latitude);
    var dLong = convertDegToRad(targetCoord.longitude - originCoord.longitude);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(convertDegToRad(originCoord.latitude)) *
            Math.cos(convertDegToRad(targetCoord.latitude)) *
            Math.sin(dLong / 2) *
            Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    if (unit === distanceUnit.k)
        return d;
    return d * 1000;
}
exports.calcDistanceToCoordinate = calcDistanceToCoordinate;
function convertDegToRad(deg) {
    return deg * (Math.PI / 180);
}
function courseCorrectionHandler(currentHeading, targetCourse, control) {
    if (currentHeading > targetCourse) {
        states_1.BOAT.setBoatTurn(control, states_1.BOAT.SPEED_HIGH, states_1.BOAT.SPEED_MAX);
    }
    else if (currentHeading < targetCourse) {
        states_1.BOAT.setBoatTurn(control, states_1.BOAT.SPEED_MAX, states_1.BOAT.SPEED_HIGH);
    }
}
exports.courseCorrectionHandler = courseCorrectionHandler;
function calcRandomCoordInsideBounds(bounds) {
    var minLat = bounds.minLat, maxLat = bounds.maxLat, minLng = bounds.minLng, maxLng = bounds.maxLng;
    var latitude = Math.random() * (maxLat - minLat) + minLat;
    var longitude = Math.random() * (maxLng - minLng) + minLng;
    return { latitude: latitude, longitude: longitude };
}
exports.calcRandomCoordInsideBounds = calcRandomCoordInsideBounds;
//# sourceMappingURL=navigationMethods.js.map