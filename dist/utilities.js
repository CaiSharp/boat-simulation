"use strict";
exports.__esModule = true;
var lodash_1 = require("lodash");
exports.trashDetectionChecker = function (trash) {
    if (!lodash_1.isEmpty(trash))
        return trash[0];
    return false;
};
//# sourceMappingURL=utilities.js.map