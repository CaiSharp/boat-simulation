"use strict";
exports.__esModule = true;
exports.testCoordinates = [
    { longitude: 52.441604, latitude: 13.627809 },
    { longitude: 52.440854, latitude: 13.627809 },
    { longitude: 52.441604, latitude: 13.627609 },
    { longitude: 52.440854, latitude: 13.627609 },
    { longitude: 52.441604, latitude: 13.627309 },
    { longitude: 52.440854, latitude: 13.627309 },
];
//# sourceMappingURL=testData.js.map