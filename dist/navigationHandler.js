"use strict";
exports.__esModule = true;
var geolib_1 = require("geolib");
var lodash_1 = require("lodash");
var index_1 = require("./navigationController/index");
var states_1 = require("./states");
var utilities_1 = require("./utilities");
var initSearchPath = function (coordinates) {
    if (!states_1.SEARCH.INIT) {
        coordinates ? states_1.SEARCH.setPath(coordinates) : states_1.SEARCH.setPath([]);
        states_1.SEARCH.setInit(true);
    }
};
var initRandomBehavior = function (value) {
    if (!value)
        return;
    states_1.RANDOM.setIsEnabled(value);
};
var boundaryHandler = function (position) {
    if (!states_1.BOUNDARY.INIT) {
        states_1.BOUNDARY.setPoints(states_1.SEARCH.PATH);
        states_1.BOUNDARY.setBounds(geolib_1.getBounds(states_1.BOUNDARY.POINTS));
        states_1.BOUNDARY.setArea([
            {
                latitude: states_1.BOUNDARY.BOUNDS.minLat - states_1.BOUNDARY.AREA_TOLERANCE,
                longitude: states_1.BOUNDARY.BOUNDS.minLng - states_1.BOUNDARY.AREA_TOLERANCE
            },
            {
                latitude: states_1.BOUNDARY.BOUNDS.minLat - states_1.BOUNDARY.AREA_TOLERANCE,
                longitude: states_1.BOUNDARY.BOUNDS.maxLng + states_1.BOUNDARY.AREA_TOLERANCE
            },
            {
                latitude: states_1.BOUNDARY.BOUNDS.maxLat + states_1.BOUNDARY.AREA_TOLERANCE,
                longitude: states_1.BOUNDARY.BOUNDS.maxLng + states_1.BOUNDARY.AREA_TOLERANCE
            },
            {
                latitude: states_1.BOUNDARY.BOUNDS.maxLat + states_1.BOUNDARY.AREA_TOLERANCE,
                longitude: states_1.BOUNDARY.BOUNDS.minLng - states_1.BOUNDARY.AREA_TOLERANCE
            },
        ]);
        states_1.BOUNDARY.setInit(true);
    }
    states_1.BOUNDARY.addToInsideCache(geolib_1.isPointInPolygon(position.getPosition(), states_1.BOUNDARY.AREA));
    if (states_1.BOUNDARY.INSIDE_CACHE.length >= states_1.BOUNDARY.INSIDE_CACHE_TOLERANCE &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.CONFIG_BOUNDARY &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.TURN_BOUNDARY &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.STRAIGHT_BOUNDARY) {
        var lastCacheValues = lodash_1.takeRight(states_1.BOUNDARY.INSIDE_CACHE, states_1.BOUNDARY.INSIDE_CACHE_TOLERANCE);
        var countedCacheValues = lodash_1.countBy(lastCacheValues);
        if (countedCacheValues["false"] >= states_1.BOUNDARY.INSIDE_CACHE_TOLERANCE / 2) {
            console.log("EXITED_SEARCH_BOUNDARY");
            states_1.NAVIGATION.setState(states_1.navigationStates.CONFIG_BOUNDARY);
        }
    }
};
var detectedTrashHandler = function (trashGroup) {
    if (states_1.NAVIGATION.STATE !== states_1.navigationStates.CONFIG_TRASH &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.TURN_TRASH &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.STRAIGHT_TRASH &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.CONFIG_BOUNDARY &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.TURN_BOUNDARY &&
        states_1.NAVIGATION.STATE !== states_1.navigationStates.STRAIGHT_BOUNDARY) {
        var trash = utilities_1.trashDetectionChecker(trashGroup);
        if (trash) {
            states_1.NAVIGATION.setState(states_1.navigationStates.CONFIG_TRASH);
        }
    }
};
var randomDirectionHandler = function () {
    if (!states_1.NAVIGATION.STATE && states_1.RANDOM.IS_ENABLED) {
        states_1.NAVIGATION.setState(states_1.navigationStates.CONFIG_RANDOM);
    }
};
var searchPathHandler = function () {
    if (!states_1.NAVIGATION.STATE &&
        states_1.SEARCH.PATH.length > 0 &&
        states_1.SEARCH.PATH_INDEX < states_1.SEARCH.PATH.length) {
        states_1.NAVIGATION.setState(states_1.navigationStates.CONFIG_SEARCH);
    }
    if (states_1.SEARCH.PATH_INDEX > states_1.SEARCH.PATH.length - 1) {
        console.log("COMPLETED_SEARCH/SEARCH_RESET");
        states_1.SEARCH.setPathIndex(0);
    }
};
exports.navigationHandler = function (_a) {
    var control = _a.control, position = _a.position, coordinates = _a.coordinates, command = _a.command, trash = _a.trash, enableRandom = _a.enableRandom;
    switch (command) {
        case "START":
            initSearchPath(coordinates);
            initRandomBehavior(enableRandom);
            states_1.POSITION.addToHistory(position.getPosition());
            boundaryHandler(position);
            detectedTrashHandler(trash);
            randomDirectionHandler();
            searchPathHandler();
            break;
        case "STOP":
            states_1.POSITION.addToHistory(position.getPosition());
            control.setPowerLeft(states_1.BOAT.SPEED_MIN);
            control.setPowerRight(states_1.BOAT.SPEED_MIN);
            break;
        case "RETURN":
            initSearchPath([coordinates[0]]);
            states_1.POSITION.addToHistory(position.getPosition());
            boundaryHandler(position);
            searchPathHandler();
            break;
        case "TEST_FORWARD":
            control.setPowerLeft(states_1.BOAT.SPEED_HIGH);
            control.setPowerRight(states_1.BOAT.SPEED_HIGH);
            break;
        case "TEST_LEFT":
            control.setPowerLeft(states_1.BOAT.SPEED_HIGH);
            control.setPowerRight(states_1.BOAT.SPEED_MIN);
            break;
        case "TEST_RIGHT":
            control.setPowerLeft(states_1.BOAT.SPEED_MIN);
            control.setPowerRight(states_1.BOAT.SPEED_HIGH);
            break;
        case "TEST_TURN_LEFT":
            control.setPowerLeft(-states_1.BOAT.SPEED_HIGH);
            control.setPowerRight(states_1.BOAT.SPEED_HIGH);
            console.log("HEADING", position.getHeading());
        case "TEST_TURN_RIGHT":
            control.setPowerLeft(states_1.BOAT.SPEED_HIGH);
            control.setPowerRight(-states_1.BOAT.SPEED_HIGH);
            console.log("HEADING", position.getHeading());
        default:
    }
    index_1.boundaryNavigationController(control, position);
    index_1.trashNavigationController({ control: control, position: position, trashGroup: trash });
    index_1.randomNavigationController(control, position);
    index_1.searchNavigationController(control, position);
};
//# sourceMappingURL=navigationHandler.js.map