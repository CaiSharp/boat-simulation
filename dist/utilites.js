"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var lodash_1 = __importDefault(require("lodash"));
exports.trashDetectionChecker = function (detector) {
    var trash = detector.getDetectedWaste();
    if (!lodash_1["default"].isEmpty(trash))
        return trash[0];
    return false;
};
//# sourceMappingURL=utilites.js.map