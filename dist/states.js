"use strict";
exports.__esModule = true;
var navigationStates;
(function (navigationStates) {
    navigationStates["CONFIG_SEARCH"] = "CONFIG_SEARCH";
    navigationStates["CONFIG_TRASH"] = "CONFIG_TRASH";
    navigationStates["CONFIG_BOUNDARY"] = "CONFIG_BOUNDARY";
    navigationStates["CONFIG_RANDOM"] = "CONFIG_RANDOM";
    navigationStates["TURN_SEARCH"] = "TURN_SEARCH";
    navigationStates["TURN_TRASH"] = "TURN_TRASH";
    navigationStates["TURN_BOUNDARY"] = "TURN_BOUNDARY";
    navigationStates["TURN_RANDOM"] = "TURN_RANDOM";
    navigationStates["STRAIGHT_POINT"] = "STRAIGHT_POINT";
    navigationStates["STRAIGHT_TRASH"] = "STRAIGHT_TRASH";
    navigationStates["STRAIGHT_BOUNDARY"] = "STRAIGHT_BOUNDARY";
    navigationStates["STRAIGHT_RANDOM"] = "STRAIGHT_RANDOM";
})(navigationStates = exports.navigationStates || (exports.navigationStates = {}));
var BOAT;
(function (BOAT) {
    BOAT.SPEED_MIN = 0;
    BOAT.SPEED_LOW = 0.4;
    BOAT.SPEED_HIGH = 0.7;
    BOAT.SPEED_MAX = 1;
    BOAT.setWeight = function (newWeight) {
        BOAT.WEIGHT = newWeight;
    };
    BOAT.setBoatStraightAhead = function (c, speed) {
        c.setPowerLeft(speed);
        c.setPowerRight(speed);
    };
    BOAT.setBoatTurnOnSpotLeft = function (c, speed) {
        c.setPowerLeft(-speed);
        c.setPowerRight(speed);
    };
    BOAT.setBoatTurnOnSpotRight = function (c, speed) {
        c.setPowerLeft(speed);
        c.setPowerRight(-speed);
    };
    BOAT.setBoatTurn = function (c, speedLeft, speedRight) {
        c.setPowerLeft(speedLeft);
        c.setPowerRight(speedRight);
    };
})(BOAT = exports.BOAT || (exports.BOAT = {}));
var NAVIGATION;
(function (NAVIGATION) {
    NAVIGATION.STATE_HISTORY = [];
    NAVIGATION.addToHistory = function (newState) {
        return NAVIGATION.STATE_HISTORY.push(newState);
    };
    NAVIGATION.STATE = null;
    NAVIGATION.setState = function (newNavigationState) {
        NAVIGATION.STATE = newNavigationState;
        NAVIGATION.addToHistory(newNavigationState);
    };
    NAVIGATION.COURSE = null;
    NAVIGATION.setCourse = function (newCourse) {
        NAVIGATION.COURSE = newCourse;
    };
    NAVIGATION.COURSE_TOLERANCE = 5;
})(NAVIGATION = exports.NAVIGATION || (exports.NAVIGATION = {}));
var SEARCH;
(function (SEARCH) {
    SEARCH.INIT = false;
    SEARCH.setInit = function (value) { return (SEARCH.INIT = value); };
    SEARCH.PATH = [];
    SEARCH.addToPath = function (newPoint) { return SEARCH.PATH.push(newPoint); };
    SEARCH.setPath = function (newPoints) { return (SEARCH.PATH = newPoints); };
    SEARCH.PATH_INDEX = 0;
    SEARCH.setPathIndex = function (newPathIndex) {
        SEARCH.PATH_INDEX = newPathIndex;
    };
    SEARCH.TARGET = null;
    SEARCH.setTarget = function (newTarget) {
        SEARCH.TARGET = newTarget;
    };
    SEARCH.TARGET_REACHED_TOLERANCE = 2.5;
})(SEARCH = exports.SEARCH || (exports.SEARCH = {}));
var POSITION;
(function (POSITION) {
    POSITION.HISTORY = [];
    POSITION.addToHistory = function (newPoint) { return POSITION.HISTORY.push(newPoint); };
})(POSITION = exports.POSITION || (exports.POSITION = {}));
var BOUNDARY;
(function (BOUNDARY) {
    BOUNDARY.INIT = false;
    BOUNDARY.setInit = function (init) { return (BOUNDARY.INIT = init); };
    BOUNDARY.setPoints = function (coordinates) {
        return (BOUNDARY.POINTS = coordinates);
    };
    BOUNDARY.setArea = function (newBounds) { return (BOUNDARY.AREA = newBounds); };
    BOUNDARY.AREA_TOLERANCE = 0.0002;
    BOUNDARY.setBounds = function (newBounds) { return (BOUNDARY.BOUNDS = newBounds); };
    BOUNDARY.INSIDE_CACHE = [];
    BOUNDARY.addToInsideCache = function (newCheck) {
        return BOUNDARY.INSIDE_CACHE.push(newCheck);
    };
    BOUNDARY.INSIDE_CACHE_TOLERANCE = 20;
    BOUNDARY.setTarget = function (newTarget) { return (BOUNDARY.TARGET = newTarget); };
    BOUNDARY.TARGET_REACHED_TOLERANCE = 5;
})(BOUNDARY = exports.BOUNDARY || (exports.BOUNDARY = {}));
var RANDOM;
(function (RANDOM) {
    RANDOM.IS_ENABLED = false;
    RANDOM.setIsEnabled = function (status) { return (RANDOM.IS_ENABLED = status); };
    RANDOM.setTarget = function (newTarget) { return (RANDOM.TARGET = newTarget); };
    RANDOM.TARGET_REACHED_TOLERANCE = 5;
})(RANDOM = exports.RANDOM || (exports.RANDOM = {}));
//# sourceMappingURL=states.js.map